import React, {Component} from 'react';
import { connect } from 'react-redux';
import {increment, decrement} from '../actions/counter';

class CounterComponent extends Component
{

    render()
    {
        const wellStyles = {maxWidth: 400, margin: '0 auto 10px'};
        const {counter, increment, decrement} = this.props;

        return(
            <div>
                <h1 className="text-center text-muted">Counter con React + Redux</h1>
                <div className="well" style={wellStyles}>
                    <button onClick={increment} >
                        Increment
                    </button>
                    <br />
                    <input type="text" value={counter} disabled />
                        <br />
                    <button onClick={decrement}>
                        Decrement
                    </button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    counter: state.counter.count
})

const mapDispatchToProps = (dispatch) => {
    return {
        increment: () => {
            dispatch(increment());
        },
        decrement: () => {
            dispatch(decrement());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CounterComponent)