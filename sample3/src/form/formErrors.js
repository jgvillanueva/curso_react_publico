import React, { Component } from 'react';

class FormErrors extends Component{
    render(){

        var errors = Object.keys(this.props.formErrors).map((item, i) => {
            return <p key={i}>{item.fieldName} {item.value}</p>
        });
            return (
                <ul className="List">
                {errors}
                </ul>
        );
    }
}

export default FormErrors;


