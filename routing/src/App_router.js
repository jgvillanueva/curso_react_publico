import React, { Component } from 'react';

import './App.css';
import Home from './home/home';
import Info from './info/info';
import Detail from './detail/detail';
import List from './list/list';
import Nav from './nav/nav';

import {Route, Redirect, Link, BrowserRouter as Router} from 'react-router-dom';

class App extends Component {
    constructor(){
        super();
    }


  render() {
      const routing = (
              <Router>
                <div>
                    <Route  path="/" component={Nav} />
                    <Route path="/home" component={Home} />
                    <Route path="/info" component={Info} />
                    <Route path="/list" component={List} />
                    <Route path="/list/detail/:nombre" component={Detail} />

                </div>
              </Router>
          )
    return (
      <div className="App">

        {routing}
      </div>
    );
  }
}

export default App;
