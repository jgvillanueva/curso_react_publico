import * as types from '../actions/actionTypes';

const initialState = {
    todoList: [{nombre:'jorge', id:0}],
    lastIndex: 0,
    todoItem: {
        nombre: 'test',
        id: 0
    },
    counter: 0
};

export default function reducer( state=initialState, action={}){
    switch(action.type){
        case types.ADD_TODO:
            state.lastIndex ++;
            state.todoList.push({nombre: action.nombre, id: state.lastIndex})
            state.todoList = state.todoList.slice();
            return{
                ...state,
            }
        case types.REMOVE_TODO:
            for (var i = 0; i < state.todoList.length; i++) {
                if (state.todoList[i].id === action.id) {
                    state.todoList.splice(i, 1);
                    break;
                }
            }
            state.todoList = state.todoList.slice();
            return{
                ...state,
            }
        case types.CHANGE_TODO:
            state.todoItem = {nombre:action.nombre, id:0};
            return{
                ...state,
            }
        default:
            return{
                ...state,
                ...action,
            }
    }
}
