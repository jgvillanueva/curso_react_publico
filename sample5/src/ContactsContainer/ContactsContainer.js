import React, { Component } from 'react';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';

import update from 'react-addons-update';


import Search from '../Search/Search';
import ContactList from '../ContactList/ContactList';

let initialData = [
    { name: "Wating", email: "data coming soon" },
];

class ContactsContainer extends Component {

    //update con $set, $push y $unshift, $splice, $merge, $apply
    constructor(){
        super();
        this.state = {
            contacts : update(initialData, {[0]:{$set: { name: "Wating", email: "..." }}}),
            filterText: ''
        }
    }

    componentDidMount(){
        fetch('./data.json')
            .then((response) => response.json()
                .then((responseData) => {
                    this.setState({contacts: responseData});
                })
            )
    }

    filterCotacts(searchText){
        this.setState({filterText:searchText});
    }


    render() {
        return (
            <div className="App" >
            <Search onInputText={this.filterCotacts.bind(this)}></Search>
        <ContactList
        contacts={this.state.contacts}
        filterText={this.state.filterText}></ContactList>
        </div>
    );
    }
}

export default ContactsContainer;

