import React, { Component } from 'react';

import './App.css';
import Home from './home/home';
import Info from './info/info';
import Detail from './detail/detail';
import List from './list/list';
import Nav from './nav/nav';
import Default from './default/default';

import {Route, Redirect, Switch, Link, NavLink, HashRouter, BrowserRouter as Router} from 'react-router-dom';


class App extends Component {
    constructor(){
        super();

        this.state = {
            data: []
        }
    }


    componentDidMount(){
        fetch("./datos.json")
            .then((response) => response.json()
                .then((responseData)=>{
                    this.setState({
                    data: responseData
                });
            })
        )
        .catch((error) => {
                console.log(error);
        })
    }

  render() {
      const renderMergedProps = (component, ...rest) => {
          const finalProps = Object.assign({}, ...rest);
          return (
              React.createElement(component, finalProps)
          );
      }
      const PropsRoute = ({ component, ...rest }) => {
          return (
              <Route {...rest} render={routeProps => {
                return renderMergedProps(component, routeProps, rest);
            }}/>
          );
      }

      const selectedStyle = {
          backgroundColor: "white",
          color: "red"
      }

      const routing = (
              <HashRouter>
                <div>
                  <nav>
                      <ul>
                          <li><NavLink to="home" exact >Home</NavLink></li>
                          <li><NavLink to="info" exact >Info</NavLink></li>
                          <li><NavLink to="list" exact >Lista</NavLink></li>
                      </ul>
                  </nav>
                  <Switch>
                      <Route exact path="/" component={Home} />
                      <Route path="/home" component={Home} />
                      <Route path="/info" component={Info} />
      <PropsRoute path='/list' component={List} data={this.state.data}></PropsRoute>
      <PropsRoute path='/detail/:nombre' component={Detail} data={this.state.data}></PropsRoute>

                      <Route component={Default} />
                  </Switch>


                </div>
              </HashRouter>
          )
    return (
      <div className="App">

        {routing}
      </div>
    );
  }
}

export default App;
