import React, { Component } from 'react';

import Contact from '../Contact/Contact';


class ContactList extends Component{
    constructor(){
        super();
    }

    render(){
        let filteredContacts = this.props.contacts.filter(
            (contact) => contact.name.indexOf(this.props.filterText) !== -1
        );
        console.log(filteredContacts);
        return (
            <ul>
                    {filteredContacts.map(
                        (contact) => <Contact key={contact.email}
                name={contact.name}
                email={contact.email} />
            )}
            </ul>
        )
    }
}

export default ContactList;