import React, { Component } from 'react';
import 'whatwg-fetch';

import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';


class App extends Component {
    constructor(){
        super();
        this.state={contacts: []};
    }

    componentDidMount(){
        fetch('./data.json')
            .then((response) => response.json()
                .then((responseData) => {
                    this.setState({contacts: responseData});
                })
            )
    }

  render() {
    return (
      <div className="App">
          <ContactsList contacts={this.state.contacts} />
      </div>
    );
  }
}

export default App;


class ContactsList extends Component {
    render(){
        if(this.props.contacts !== undefined){
            var items = this.props.contacts.map((item, i) => {
                return <Contact nombre={item.name} edad={item.age} key={i}></Contact>
            });
        }
        return (
            <ul className="List">
            {items}
            </ul>
        )
    }
}

class Contact extends Component {
    constructor(){
        super();
    }

    render(){
        return (
            <div>{this.props.nombre} : {this.props.edad}</div>
        )
    }
}