import React, { Component } from 'react';
import { connect } from 'react-redux';
import { removeTodo } from '../actions/todoActions';


class TodoItem extends Component {
    removeTodo(id){
        this.props.removeTodo({
            id: id
        });
    }

    render() {
        return (
            <div className="todoItem">
                <h2>{this.props.nombre}</h2>
                <button onClick={this.removeTodo.bind(this, this.props.id)}>Delete</button>
            </div>
        );
    }
}

const mapStateToProps = state => ({  })
const mapDispatchToProps = (dispatch) => {
    return {
        removeTodo: (data) => {
            dispatch(removeTodo(data));
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TodoItem)
