import React, { Component } from 'react';
import './App.css';

import TodoList from './components/todoList';
import Form from './components/form';

import { connect } from 'react-redux';




class App extends Component {
  render() {
    return (
      <div className="App">
        <h1>Lista de ToDos</h1>

          <TodoList></TodoList>
          <Form></Form>
      </div>
    );
  }
}

export default connect()(App)

