import React, { Component } from 'react';
import ColorAppStore from "../stores/ColorAppStore";


class Caja extends Component{
    constructor(props) {
        super(props);
        this.state = {
            color: ColorAppStore.getActiveColor()
        }
    }

    componentDidMount() {
        ColorAppStore.on("storeUpdated", this.updateBackgroundColor);
    }

    componentWillUnmount() {
        ColorAppStore.removeListener("storeUpdated", this.updateBackgroundColor);
    }

    updateBackgroundColor = () => {
        this.setState({
              color: ColorAppStore.getActiveColor()
        })
    };

    render(){
        return (
            <div>
                <div className="color-container" style={{backgroundColor: this.state.color}}/>
            </div>
        )
    }
}

export default Caja