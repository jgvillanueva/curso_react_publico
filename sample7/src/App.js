import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
    constructor(){
        super();
        let color = window.location.hash.substr(1);
        if(color == ""){
            color = '#000000';
        }
        this.state = {
            style: {
                color: '#'+color
            },
        }
        window.addEventListener('hashchange', () => {
            this.setState({
                style: {
                    color: window.location.hash.substr(1)
                },
            });
        });
    }
  render() {

    return (
      <div className="App">
            <h1 style={this.state.style}>Mi color</h1>
      </div>
    );
  }
}

export default App;
