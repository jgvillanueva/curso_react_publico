import React, { Component } from 'react';

import {Route, Redirect, Link, BrowserRouter as Router} from 'react-router-dom';


class List extends Component{

    constructor(){
        super();
        console.log("componente lista");
    }


    goDetail(item){
        this.props.history.push("/detail/"+item.nombre);
    }

    render(){

        const listado = this.props.data.map((item, index)=>
            <li key={item.nombre + index}>{item.nombre}
                <button onClick={this.goDetail.bind(this, item)}>Detalle</button>
            </li>
        )
        return (
            <div className="lista">
                <h1>Lista</h1>
                <ul>
                 {listado}
                </ul>
            </div>
        )
    }
}

export default List