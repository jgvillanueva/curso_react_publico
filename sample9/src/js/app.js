import React, { Component } from "react";
import ReactDOM from "react-dom";
class App extends Component {
    constructor() {
        super();
        this.state = {
            title: ""
        };
    }
    render() {
        return (
            <h2>{this.state.title}</h2>
         );
    }
}
export default App;

const wrapper = document.getElementById("app");
wrapper ? ReactDOM.render(<App />, wrapper) : false;