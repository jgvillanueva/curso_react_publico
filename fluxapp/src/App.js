import React, { Component } from 'react';
import './App.css';

import  Botonera from './botonera/botonera';
import  Caja from './caja/caja';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Botonera></Botonera>
                <Caja></Caja>
            </div>
        );
    }
}

export default App;
