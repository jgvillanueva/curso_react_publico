import React, { Component } from 'react';
import { connect } from 'react-redux';

import './App.css';

import { counter } from './actions/counter';
import  CounterComponent  from './components/counterComponent';




class App extends Component {
  render() {
    return (
      <div className="App">
        <CounterComponent></CounterComponent>
      </div>
    );
  }
}

export default App;
