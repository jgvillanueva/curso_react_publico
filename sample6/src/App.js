import React, { Component } from 'react';
import PropTypes from 'prop-types';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <MiComponente miTexto="Hola mundo" miCustom={15}></MiComponente>
      </div>
    );
  }
}

export default App;


class MiComponente extends Component {
    render() {
        return (
            <div className="MiComponente">
            {this.props.miTexto} - {this.props.porDefecto}
            <div>Valor custom: {this.props.miCustom}</div>
            </div>
        );
    }
}

let validacionCustom = (props, propName, componentName) => {
    if(props[propName]){
        let value = props[propName];
        if(value < 10)
            return new Error(
                `${propName} en ${componentName} no debe tener menos de 10 caracteres`
            );
    }
}

MiComponente.propTypes = {
    miTexto: PropTypes.string.isRequired,
    miCustom:   validacionCustom
}
// este falla por número    <MiComponente miTexto={5}></MiComponente>
// este falla por falta de parámetro    <MiComponente ></MiComponente>
// este falla por tener un valor bajo    <MiComponente miTexto="Hola mundo" miCustom={5}></MiComponente>
MiComponente.defaultProps={
    porDefecto: "Mi valor por defecto"
}

