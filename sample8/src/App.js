import React, { Component } from 'react';

import { Route, Link, BrowserRouter as Router } from 'react-router-dom';

import Settings from './settings/settings';
import About from './about/about';
import Home from './home/home';

import './App.css';

class App extends Component {


  render() {
      const routing = (
          <Router>
              <div>
                <Route path="/" component={Home} />
                <Route path="/about" component={About} />
                <Route path="/settings" component={Settings} />
              </div>
          </Router>
      )

    return (
      <div className="App">
        <ul>
          <li><a href="/">Home</a></li>
          <li><a href="/about">About</a></li>
          <li><a href="/settings">Settings</a></li>
          </ul>
          {routing}
      </div>
    );
  }
}

export default App;
