import React, { Component } from 'react';
import {Route, Redirect, Link, BrowserRouter as Router} from 'react-router-dom';


class Nav extends Component {
    render(){
        return (
            <div>
            <nav>
                <ul>
                    <li><Link to="/home">Home</Link></li>
                    <li><Link to="/info">Info</Link></li>
                    <li><Link to="/list">Lista</Link></li>
                </ul>
            </nav>
        </div>
    )
    }
}

export default Nav;
