import * as types from './actionTypes';

export function addTodo (data){
    return{
        type: types.ADD_TODO,
        nombre: data.nombre
    }
}

export function removeTodo (data){
    return{
        type: types.REMOVE_TODO,
        id: data.id
    }
}


export function changeTodo (data){
    return{
        type: types.CHANGE_TODO,
        nombre: data.nombre
    }
}
