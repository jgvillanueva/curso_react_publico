import React, { Component } from 'react';

import { connect } from 'react-redux';
import { addTodo, changeTodo } from '../actions/todoActions';




class Form extends Component {

    sendForm(event){
        event.preventDefault();

        const {addTodo} = this.props;

        addTodo({
            nombre: event.target.nombre.value
        });
    }

    inputChange(event){
        const {changeTodo} = this.props;
        changeTodo({
            nombre: event.target.value
        })
    }

    componentDidMount(){

    }

    render() {
        const {todoItem} = this.props;
        return (
            <div className="form">
                <form  onSubmit={this.sendForm.bind(this)}>
                    <div>
                        <label htmlFor='nombre'>Nombre</label>
                        <input name='nombre' onChange={this.inputChange.bind(this)}/>
                    </div>
                    <button type="submit" className="btn btn-primary submit">Add</button>
                </form>

        {todoItem.nombre}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    todoItem: state.todoReducers.todoItem,
})
const mapDispatchToProps = (dispatch) => {
    return {
        addTodo: (data) => {
            dispatch(addTodo(data));
        },
        changeTodo: (data) => {
            dispatch(changeTodo(data));
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Form)
