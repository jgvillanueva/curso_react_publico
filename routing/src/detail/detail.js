import React, { Component } from 'react';

class Detail extends Component{
    constructor(props){
        super(props);
        this.state = {
            nombre: ''
        }
    }

    componentDidMount(){
        console.log(this.props.data);

    }

    volver(){
        this.props.history.goBack();
    }
    render(){
        const data = this.props.data.filter((item) => {
            return item.nombre === this.props.match.params.nombre;
        });
        let nombre = "";
        if( data.length > 0 ) {
            nombre = data[0].nombre;
        }

        return (
            <div>
                <h1>DETAIL: {nombre}</h1>
                <button onClick={this.volver.bind(this)}>Volver</button>
            </div>

        )
    }
}

export default Detail