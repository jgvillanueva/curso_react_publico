import React, { Component } from 'react';

class Search extends Component{
    constructor(){
        super();
    }

    handleChange(event){
        this.props.onInputText(event.target.value)
    }

    render(){
        return (
            <div className='form-group' >
                <label htmlFor='search' >Search</label>
                    <input type='text' placeholder="search"
                            className='form-control' name='search'
                        onChange={this.handleChange.bind(this)}/>
            </div>
        )
    }
}

export default Search;