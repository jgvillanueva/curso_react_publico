import React, { Component } from 'react';

import * as ColorAppActions from "../actions/ColorAppActions";


class Botonera extends Component{

    onButtonclick(colorName){
        ColorAppActions.changeColor(colorName)
    }
    render(){
        return (
            <ul>
                <button onClick={this.onButtonclick.bind(this, '#ff0000')}>Rojo</button>
                <button onClick={this.onButtonclick.bind(this, '#00ff00')}>Verde</button>
            </ul>
        )
    }
}

export default Botonera