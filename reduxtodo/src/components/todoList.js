import React, { Component } from 'react';

import TodoItem from './todoItem';

import { connect } from 'react-redux';


class TodoList extends Component {
    render() {
        const {todoList} = this.props;
        const lista = todoList.map((item, index) =>
            <TodoItem nombre={item.nombre} id={item.id} key={item.id}></TodoItem>
            )
        return (
            <div className="todoList">
            {lista}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    todoList: state.todoReducers.todoList
})
const mapDispatchToProps = (dispatch) => {
    return {
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TodoList)